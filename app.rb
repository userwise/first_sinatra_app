require 'sinatra'
require 'pry' 

get '/' do 
  @name = params[:name]
  @city = params[:city]

  erb :home
end

get '/code' do
  erb :code
end

get '/nycda' do
  'Awesome coding school with zero pretzels'
end

get '/login' do 
  erb :login
end

post '/login' do
  binding.pry
end

